import threading
import numpy as np
#from playsound import playsound
from os.path import join, realpath, dirname
import sys, os, time
import readchar
from gpiozero import Button, LED, TonalBuzzer
from gpiozero.tones import Tone

sequence = np.random.randint(4, size=10) #sequence to guess
print(sequence)
stage = 3 #stage that determines how long is the sequene played
phase = "singing" #"singing" is Simon singing "guessing" is player guessing
simon_count = 0 #counter to iterate in the sequence played
player_count = 0 #counter to keep count of player guess phase
blink_timeout = None
deblink_timeout = None
restartsequence_timeout = None

r_button_thread = None
g_button_thread = None
b_button_thread = None
y_button_thread = None

tonal_buzzer = TonalBuzzer(6)

r_button = Button(17)
g_button = Button(27)
b_button = Button(22)
y_button = Button(23)

leds = {
	"r_led" : LED(25),
	"g_led" : LED(26),
	"b_led" : LED(24),
	"y_led" : LED(16)
}

sounds = {
	"r_led": 220,
	"g_led": 220 * 10/8,
	"b_led": 220 * 15/8,
	"y_led": 220 * 18/8
}

leds_debug = {
	"r_led": 0,
	"g_led": 0,
	"b_led": 0,
	"y_led": 0
}


def blink(led = None):
	global phase, simon_count, deblink_timeout, restartsequence_timeout
	if phase == "singing":
		if simon_count < stage:
			leds[list(leds.keys())[sequence[simon_count]]].on()
			tonal_buzzer.play(sounds[list(sounds.keys())[sequence[simon_count]]])
			leds_debug[list(leds_debug.keys())[sequence[simon_count]]] = 1
			print("r_led: {} | g_led: {} | b_led: {} | y_led: {}".format(leds_debug["r_led"], leds_debug["g_led"], leds_debug["b_led"], leds_debug["y_led"]))
			#playsound(sounds[list(leds.keys())[sequence[simon_count]]])
			simon_count += 1
			deblink_timeout = threading.Timer(0.5, deblink)
			deblink_timeout.start()
		else:
			print("restarting sequence")
			simon_count = 0
			restartsequence_timeout = threading.Timer(2.0, blink)
			restartsequence_timeout.start()
	elif phase == "guessing":
		leds[led].on()
		tonal_buzzer.play(sounds[led])
		deblink_timeout = threading.Timer(0.5, deblink)
		deblink_timeout.start()


def deblink():
	global phase, blink_timeout, r_button_thread, g_button_thread, b_button_thread, y_button_thread
	if phase == "singing":
		tonal_buzzer.stop()
		for i in leds_debug.keys():
			leds_debug[i] = 0
		for j in leds.keys():
			leds[j].off()
		print("r_led: {} | g_led: {} | b_led: {} | y_led: {}".format(leds_debug["r_led"], leds_debug["g_led"], leds_debug["b_led"], leds_debug["y_led"]))
		blink_timeout = threading.Timer(1.0, blink)
		blink_timeout.start()
	elif phase == "guessing":
		tonal_buzzer.stop()
		for j in leds.keys():
			leds[j].off()

w = threading.Condition()


RED_PRESSED = False
GREEN_PRESSED = False
BLUE_PRESSED = False
YELLOW_PRESSED = False

from time import sleep

def red_pressed():
	global phase
	if phase == "singing":
		phase = "guessing"
		blink_timeout.cancel()
		deblink_timeout.cancel()
		restartsequence_timeout.cancel()
	global RED_PRESSED
	RED_PRESSED = True
	with w:
		w.notify_all()	

def red_released():
	global RED_PRESSED
	RED_PRESSED = False


def green_pressed():
	global phase
	if phase == "singing":
		phase = "guessing"
		blink_timeout.cancel()
		deblink_timeout.cancel()
		restartsequence_timeout.cancel()
	global GREEN_PRESSED
	GREEN_PRESSED = True
	with w:
		w.notify_all()	

def green_released():
	global GREEN_PRESSED
	GREEN_PRESSED = False

def blue_pressed():
	global phase
	if phase == "singing":
		phase = "guessing"
		blink_timeout.cancel()
		deblink_timeout.cancel()
		restartsequence_timeout.cancel()
	global BLUE_PRESSED
	BLUE_PRESSED = True
	with w:
		w.notify_all()	

def blue_released():
	global BLUE_PRESSED
	BLUE_PRESSED = False

def yellow_pressed():
	global phase
	if phase == "singing":
		phase = "guessing"
		blink_timeout.cancel()
		deblink_timeout.cancel()
		restartsequence_timeout.cancel()
	global YELLOW_PRESSED
	YELLOW_PRESSED = True
	with w:
		w.notify_all()	

def yellow_released():
	global YELLOW_PRESSED
	YELLOW_PRESSED = False

if __name__ == '__main__':
	r_button.when_pressed = red_pressed
	r_button.when_released = red_released
	g_button.when_pressed = green_pressed
	g_button.when_released = green_released
	b_button.when_pressed = blue_pressed
	b_button.when_released = blue_released
	y_button.when_pressed = yellow_pressed
	y_button.when_released = yellow_released
	if phase == "singing":
		blink_timeout = threading.Timer(1.0, blink)
		blink_timeout.start()

	while 1:
		with w:
			w.wait()
		if RED_PRESSED:
			blink(led = "r_led")
			print("RED")
		elif GREEN_PRESSED:
			blink(led = "g_led")
			print("GREEN")
		elif BLUE_PRESSED:
			blink(led = "b_led")
			print("BLUE")
		elif YELLOW_PRESSED:
			blink(led = "y_led")
			print("YELLOW")
		# sleep(0.3)