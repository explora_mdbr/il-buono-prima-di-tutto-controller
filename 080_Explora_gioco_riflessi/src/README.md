# Explora PARI - Gioco dei riflessi

### Installation:

-install raspbian
https://www.raspberrypi.org/downloads/raspbian/

(Tested with Raspbian Buster with desktop)

- Configuring the keyboard first will make everything easier:
`sudo raspi-config`
 Localization options > Keyoboard Layout > (your keyboard)

-run the installation script. Be sure to have internet access
`install.sh`

It will give you the randon generated password for the DB,
copy it and paste it in `database_manager.py`

Then restart the Raspberry with

`sudo reboot`

