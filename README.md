# Explora Centrale del latte 

### Installation:

-install raspbian
https://www.raspberrypi.org/downloads/raspbian/

(Tested with Raspbian Buster with desktop)

-add the `on_boot.sh` script to `/etc/rc.local`

-run the installation script. Be sure to have internet access
`install.sh`

Then restart the Raspberry with

`sudo reboot`

