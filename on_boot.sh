#!/bin/sh -e

tt=$(date +%Y-%M-%d-%T)

echo DATE: $tt

LOG_DIR="/home/pi/logs"
log_http="$LOG_DIR/$(echo $tt)_log_http.log"
err_http="$LOG_DIR/$(echo $tt)_err_http.log"

OLD_LOG_DIR="$LOG_DIR/old"
mkdir -p $OLD_LOG_DIR

log_led="$LOG_DIR/$(echo $tt)_log_led.log"
err_led="$LOG_DIR/$(echo $tt)_err_led.log"

set +e
mv $LOG_DIR/*.log $OLD_LOG_DIR
set -e

#start LED SERVER
python /home/pi/ws_server/display_server.py -b 44 &

#start HTTP SERVER

python3 /home/pi/buono_server/main.py >$log_http 2>$err_http &
echo HTTP LOG to $log_http
