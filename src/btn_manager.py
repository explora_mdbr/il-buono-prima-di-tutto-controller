import threading

from os.path import join, realpath, dirname
import random
import sys, os, time
from gpiozero import Button, LED
from sys import stdout
# from gpiozero.tones import Tone


# =====================================
# =           CONFIGURATION           =
# =====================================
# ROUND_DURATION = 30
# GENERIC_DELAY = 2
# DELAY_GAMEOVER = 5
# MAX_BUTTONS = 6

# LED_ON_DURATION = 1
DEBOUNCE = 0.1
# ======  End of CONFIGURATION  =======


# ===============================
# =           GLOBALS           =
# ===============================
# tonal_buzzer = TonalBuzzer(6)

#GPIO NUMBERS, no physical ones
buttons = [ Button(27), Button(9), Button(6),
		Button(19), Button(24), Button(20) ]
# leds = { 17:LED(25), 23:LED(26), 27:LED(24), 22:LED(16) }
# leds = [ LED(17), LED(10), LED(5),
# 		LED(13), LED(23), LED(16) ]

# ======  End of GLOBALS  =======

def dump_to_console(*args):
	args = [str(a) for a in args]
	print("[BTN MANAGER]", " ".join(args))
	stdout.flush()


class Button_Manager:
	def __init__(self):
		self.start_at = time.time()
		self.last_button = -1
		self.available = False

		self.button_handlers = []
		self.last_btn_press = []

		global buttons
		for i,btn in enumerate(buttons):
			self.button_handlers.append(self.default_handler)
			self.last_btn_press.append(self.start_at)
			btn.when_pressed = self.press_button 

		dump_to_console('Button manager started')

	def __del__(self):
		dump_to_console("Button manager ended")

	def press_button(self, button):
		number = buttons.index(button)

		global DEBOUNCE
		now = time.time()
		if now - self.last_btn_press[number] > DEBOUNCE:
			self.last_btn_press[number] = now
			self.button_handlers[number](number)
		# else:
		# 	print("BOUNCE")
		

	def default_handler(self, btn_n):
		print("Handler for button {} not set".format(btn_n))

	
	def set_handler(self, n, han):
		if n >= len(self.button_handlers):
			raise IndexError("Setting handler for button failed: Only {} buttons allowed".format(len(self.button_handlers)))

		self.button_handlers[n] = han

		dump_to_console('Added handler {} on button {}'.format(han.__name__, n ))
		# if not self.available: return
		# self.available = False
		# if number == self.last_button:
		# 	light_led(number)
		# 	self.score += 1
		# 	print("========= SCORE %s ========== (+1)! " % self.score)
		# 	time.sleep(GENERIC_DELAY)
		# 	self.light_button()
		# else:
		# 	light_all(number)
		# 	self.available = True
		# 	print("###### SCORE %s ######" % self.score)




# def countdown(counter):
# 	while 1:
# 		counter -= 1
# 		print("-"*counter , counter)
# 		time.sleep(1)
# 		if counter < 1 : return

if __name__ == '__main__':
	btn = Button_Manager()

	from time import sleep
	while 1:
		sleep(0.01)