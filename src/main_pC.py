#!/usr/bin/env python3

import os 
import signal
from sys import exit
import datetime
import time
import json
import threading
from sys import stdout
import random
import math

import ws_server
import http_server
import defaults
import btn_manager
import random

dir_path = os.path.dirname(os.path.realpath(__file__))
http_server.STATIC_DIR = os.path.join(dir_path, "public/pC")
print("STATIC DIR:", http_server.STATIC_DIR)


def dump_to_console(*args):
	args = [str(a) for a in args]
	print("[MAIN]", " ".join(args))
	stdout.flush()

def get_control_json(state, count = 0):
	return json.dumps({
		"msg": "",
		"state": state,
		"count": count
		})


cv = threading.Condition()

# ===============================
# =           GLOBALS           =
# ===============================
MAX_TIME_VIDEO =  20

VID_NUM = 5 #MUST ALSO CHANGE in main.js

last_button_press = time.time()

#Global delay for automatic adjusments
#this control temp decrease, led refresh rate, and others
AUTO_MOD_DELAY = 0.1

# ======  End of GLOBALS  =======


# =====================================
# =           STATE MACHINE           =
# =====================================
t = 0
state = None
state_n = 0

# def auto_mod_state():
# 	global t, state, MAX_TIME_VIDEO, VID_NUM, state_n

# 	while AUTO_MOD_DELAY:
# 		# print(state, time.time() - last_button_press)
# 		# t += AUTO_MOD_DELAY
# 		time.sleep(AUTO_MOD_DELAY)

# 		if state != "pC_TEXT":
# 			if time.time() - last_button_press > MAX_TIME_VIDEO:
# 				state = "pC_TEXT"
# 				state_n = (state_n + 1) % VID_NUM

# 				with cv:
# 					cv.notify()
# 		# if state == "pA_GIGANT":
# 		# 	if t > MAX_TIME_VIDEO:
# 		# 		t = 0
# 		# 		ws_server.send(get_control_json("pA_INIT", "INIT (time)"))
# 		# 		state = "pA_INIT"


	
texts = [ "Avvia il confezionamento premendo questo pulsante", 
"Riempi la bottiglia premendo questo pulsante", 
"Tappa la bottiglia premendo questo pulsante", 
"Etichetta la bottiglia premendo questo pulsante", 
"Stampa la data di scadenza sulla bottiglia premendo questo pulsante" ]

texts = texts[:VID_NUM] #trim texts list to the Number of videos (just to be sure)

def handle_state():
	global t, state, state_n


	if state == "pC_TEXT":
		ws_server.send(get_control_json("pC_TEXT", texts[state_n % len(texts)]))
		# time.sleep(2)
		state = "pC{}".format(state_n)
		return

	dump_to_console("HANDLING STATE:", state)

	ws_server.send(get_control_json(state, state_n))

	# time.sleep(1)


# ======  End of STATE MACHINE  =======

def button_handler(n):
	global cv, last_button_press, state_n, state
	dump_to_console("======= pressed button", n)
	last_button_press = time.time()
	state_n = ((n + VID_NUM)  % VID_NUM ) + 1 
	state = "pC{}".format(n)
	with cv:
		cv.notify()

def main():
	import argparse
	global MAX_TIME_VIDEO

	parser = argparse.ArgumentParser(description='Explora\'s Il buono prima di tutto controller' )
	parser.add_argument('-t','--time',type=int, default=MAX_TIME_VIDEO,  help='Seconds on each round')
	parser.add_argument('--socket', '-s', nargs='?', default='/tmp/display-server-socket') 
	args = parser.parse_args()
	
	MAX_TIME_VIDEO  = args.time

	#START HTTP SERVER	
	http_server.start()
	dump_to_console("Explora\'s Il buono prima di tutto HTTP server started at port {}\non {}"\
		.format(http_server.PORT, datetime.datetime.now()))
	
	#START WS SERVER
	def handle_msg(self):
		global state, cv
		msg = self.data

		dump_to_console("pC received WS:", msg)

		state = "pC_TEXT"

		with cv:
			cv.notify()

	ws_server.Pari_Websocket_Handler.handleMessage = handle_msg

	ws_server.start()
	dump_to_console("Explora\'s Il buono prima di tutto Websocket server started at port {}\non {}"\
		.format(ws_server.WS_PORT, datetime.datetime.now()))
	

	#button manager
	bman = btn_manager.Button_Manager()
	bman.set_handler(0, button_handler)
	bman.set_handler(1, button_handler)
	bman.set_handler(2, button_handler)
	bman.set_handler(3, button_handler)
	bman.set_handler(4, button_handler)
	bman.set_handler(5, button_handler)


	#Automatic state modification
	# t = threading.Thread(target=auto_mod_state)
	# t.start()

	#HANDLE SIGTERM
	def close_sig_handler(signal, frame):
		dump_to_console("\nInterrupt signal received, cleaning up..")
		ws_server.close()
		http_server.close()
		global AUTO_MOD_DELAY
		AUTO_MOD_DELAY = 0
		t.join()
		exit()

	signal.signal(signal.SIGINT, close_sig_handler)

	
	# ws_server.send(get_control_json("pA_INIT", "FIRST"))
	global state
	state = "pC_TEXT"

	while 1:
		with cv:
			cv.wait()
		
		handle_state()

		
		

	# PLAY LOOP

	# while 1:
		
	# 	the_code = ""
	# 	while 1:
	# 		c = f.read(1)
	# 		if ord(c) == 40: break

	# 		if ord(c):
	# 			the_code += str(code_input_to_number(ord(c)))
	# 			stdout.flush()


	# 	db_error = code_to_db(the_code)
	# 	if db_error:
	# 		dump_to_console(db_error)
	# 		continue


	# 	dump_to_console("Scanned code", the_code)

	# 	ws_server.send(get_control_json("INSTRUCTIONS"))
	# 	# dump_to_console("INSTRUCTIONS screen, waiting ", delays["INSTRUCTIONS"], "seconds")
	# 	time.sleep(delays["INSTRUCTIONS"])

	# 	for i in range(delays["PRE_COUNT"]):
	# 		ws_server.send(get_control_json("PRE_COUNT", str(delays["PRE_COUNT"] - i)))
	# 		time.sleep(1)

	# 	ws_server.send(get_control_json("PLAY"))
	# 	# dump_to_console("PLAY screen, waiting ", delays["PRE_COUNT"], "seconds")
	# 	time.sleep(delays["PLAY"])

	# 	t = threading.Thread(target=countdown, args=(args.time,))
	# 	t.start()
	# 	# -----------  Start  -----------
		 
	# 	game = riflessi.Riflessi_Game()
	# 	for i,btn in enumerate(riflessi.buttons):
	# 		btn.when_pressed = game.press_button 
		
	# 	game.light_button()

	# 	# -----------  PLAY  -----------
		
	# 	t.join() #--- Block here ---

	# 	# -----------  END  -----------
	# 	ws_server.send(get_control_json("FINAL", str(game.score)))
	# 	del(game)
	# 	# dump_to_console("FINAL screen, waiting ", delays["FINAL"], "seconds")
		
	# 	time.sleep(delays["FINAL"])

	# 	for i,btn in enumerate(riflessi.buttons):
	# 		btn.when_pressed = riflessi.no_button 


	


if __name__ == '__main__':
	main()