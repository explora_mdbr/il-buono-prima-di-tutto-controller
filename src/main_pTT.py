#!/usr/bin/env python3

import os 
import signal
from sys import exit
import datetime
import time
import json
import threading
from sys import stdout
import random
import socket
from struct import pack
import math

import ws_server
import http_server
import defaults
import btn_manager


dir_path = os.path.dirname(os.path.realpath(__file__))
http_server.STATIC_DIR = os.path.join(dir_path, "public/pTT")


def dump_to_console(*args):
	args = [str(a) for a in args]
	print("[MAIN]", " ".join(args))
	stdout.flush()

def get_control_json(state, count = 0):
	return json.dumps({
		"msg": "",
		"state": state,
		"count": count
		})


cv = threading.Condition()
import random

# ===============================
# =           GLOBALS           =
# ===============================
MAX_TEMP = 75
current_temp = 0.0
#decrease temp, relative to how hot it is
DECREASE_TEMP_REL = 7
#decrese temp, constant factor
DECREASE_TEMP_CONST = 0.25
#time after each press to wait before decrease temp
DECREASE_TEMP_WAIT = 1

#Global delay for automatic adjusments
#this control temp decrease, led refresh rate, and others
AUTO_MOD_DELAY = 0.1

TEMP_HOLD_SECS = 15
TEMP_HOLD_SECS_DURATION = 0.5
MAX_TEMP_HOLD_ATTEMPS = 3
current_temp_holding_time = TEMP_HOLD_SECS 

HOT_TEMP_HOLD_SIN_DIM = 0.4
HOT_TEMP_HOLD_SIN_VEL = 4
WHITE_MILK_FLOW_SPEED = 5
WHITE_DARK_COLOR = 0x26251F
WHITE_BRIGHT_COLOR = 0xDFD9B4


FINAL_SCREEN_SECS = 5

#buttons
last_button_press = time.time()
button = btn_manager.buttons[0]

#led server
N_LEDS_TEMP = 22
N_LEDS_FLOW_LEFT = 9
N_LEDS_FLOW_RIGHT = 9

N_LEDS_USED = N_LEDS_TEMP + N_LEDS_FLOW_LEFT + N_LEDS_FLOW_RIGHT
N_LEDS_BUFFER = 44

assert  N_LEDS_USED <= N_LEDS_BUFFER
streamFormat = b"<" + b"I"*N_LEDS_BUFFER
# ======  End of GLOBALS  =======


# ===================================
# =           LED CONTROL           =
# ===================================

t = 0
sock = None

def get_temp_color(temp):
	temp_color = int(temp * 0xff)
	final_color = 0x000000 | (temp_color <<16) | (255-temp_color)
	final_color = min(final_color, 0xffffff)
	return final_color

def get_basic_temp_array(temp):
	final_color = get_temp_color(temp)
	return [final_color for i in range(N_LEDS_TEMP)]

def get_waiting_hot_array():
	global t
	wave = math.sin(t*HOT_TEMP_HOLD_SIN_VEL) * HOT_TEMP_HOLD_SIN_DIM + 1 - HOT_TEMP_HOLD_SIN_DIM
	temp_color = int( wave * 0xff )
	final_color = 0x000000 | (temp_color <<16) 
	return [final_color] * N_LEDS_TEMP
	

def get_temp_array(temp, state):
	global t
	temp_array = get_basic_temp_array(temp)

	if state == "pTT_HOLDING":
		return get_waiting_hot_array()

	GRADIENT_SUBD = 3
	temp_step = (1 - temp) / GRADIENT_SUBD

	up_temp_index = int(temp * N_LEDS_TEMP)

	temp_colors = [ get_temp_color(temp + temp_step*subd) for subd in range(GRADIENT_SUBD) ]
	# print(up_temp_index, "{:x} {:x} {:x}".format(*temp_colors))

	for i in range(up_temp_index):
		temp_array[i] = 0xff0000
 	
	for j, color in enumerate(temp_colors):
		index = up_temp_index - j
		if index < 0:
			continue

		try:
			temp_array[index + 1] = color
			# temp_array[index + 1] = 0x00ff00
		except IndexError as e:
			pass


	return temp_array

def get_control_led_milk_array(temp, state):
	global t, WHITE_MILK_FLOW_SPEED
	global current_temp

	temp_array = get_temp_array(temp, state)

	flow_left_c = WHITE_DARK_COLOR
	flow_right_c = WHITE_DARK_COLOR

	if state == "pTT_TEMP_CONTROL" and current_temp:
		flow_left_c = WHITE_BRIGHT_COLOR 

	if state == "pTT_FINAL":
		flow_right_c = WHITE_BRIGHT_COLOR

	posr = int( t * WHITE_MILK_FLOW_SPEED) % (N_LEDS_FLOW_RIGHT + 3)
	posl = int( t * WHITE_MILK_FLOW_SPEED) % (N_LEDS_FLOW_LEFT + 3)
	
	flow_right = [ flow_right_c ] * min(posr, N_LEDS_FLOW_RIGHT )
	flow_left = [ flow_left_c ] * min(posl, N_LEDS_FLOW_LEFT )
	
	flow_right = [ WHITE_DARK_COLOR ] * (N_LEDS_FLOW_RIGHT - posr ) + flow_right
	flow_left = [ WHITE_DARK_COLOR ] * (N_LEDS_FLOW_LEFT - posl ) + flow_left

	flow_left.reverse()
	flow_right.reverse()
	
	return temp_array + flow_right + flow_left + [0]*(N_LEDS_BUFFER - N_LEDS_USED)


def send_to_leds(temp, state):
	global sock, streamFormat
	if not sock:
		return

	MESSAGE = get_control_led_milk_array(temp/MAX_TEMP, state)
	sock.send(pack(streamFormat, *MESSAGE))

# ======  End of LED CONTROL  =======



# =====================================
# =           STATE MACHINE           =
# =====================================
state = None
temp_hold_attemps = MAX_TEMP_HOLD_ATTEMPS 

def auto_mod_state():
	global DECREASE_TEMP_CONST, AUTO_MOD_DELAY, MAX_TEMP
	global current_temp, state, last_button_press, current_temp_holding_time
	global temp_hold_attemps, MAX_TEMP_HOLD_ATTEMPS 
	global t


	while AUTO_MOD_DELAY:
		# print(state)
		t += AUTO_MOD_DELAY
		time.sleep(AUTO_MOD_DELAY)

		send_to_leds(current_temp, state)

		if current_temp == 0:
			continue

		if state == "pTT_TEMP_CONTROL" :
			if time.time() - last_button_press > DECREASE_TEMP_WAIT:
				current_temp -= (current_temp / MAX_TEMP) * DECREASE_TEMP_REL * AUTO_MOD_DELAY
				current_temp -= DECREASE_TEMP_CONST * AUTO_MOD_DELAY
				current_temp = max(0, current_temp)

				if current_temp == 0:
					state = "pTT_RESTART"
					with cv:
						cv.notify()
					continue

				ws_server.send(get_control_json(state, "{:.2f}".format(current_temp) ))
				temp_hold_attemps = MAX_TEMP_HOLD_ATTEMPS 
				continue

		if state == "pTT_HOLDING" :
			ws_server.send(get_control_json("pTT_HOLDING", int(current_temp_holding_time)))
			
			if button.is_pressed:
				current_temp_holding_time -= AUTO_MOD_DELAY / TEMP_HOLD_SECS_DURATION
			else:
				current_temp_holding_time = TEMP_HOLD_SECS
				temp_hold_attemps -= 1
				if temp_hold_attemps == 0:
					state = "pTT_RESTART"
					with cv:
							cv.notify()
				else:
					state = "pTT_ASK_TO_HOLD"
					send_to_leds(0, state)
					ws_server.send(get_control_json("pTT_ASK_TO_HOLD", "Try again")) 
					time.sleep(0.2)
					send_to_leds(current_temp, state)

				continue

			if current_temp_holding_time <= 0:
				state = "pTT_FINAL"
				ws_server.send(get_control_json("pTT_FINAL"))
				SEC_SUBDIVISION = 10
				N_SEC_SUBDIVISION = FINAL_SCREEN_SECS*SEC_SUBDIVISION 
				
				for i in range(N_SEC_SUBDIVISION):
					t += 1 / SEC_SUBDIVISION
					current_temp -= MAX_TEMP / N_SEC_SUBDIVISION
					send_to_leds(current_temp, state)
					time.sleep(1/SEC_SUBDIVISION)
				
				state = "pTT_PREMI"
				with cv:
					cv.notify()


def handle_state():
	global state, current_temp, MAX_TEMP, current_temp_holding_time, TEMP_HOLD_SECS
	global temp_hold_attemps, MAX_TEMP_HOLD_ATTEMPS 
	global t

	if state == "pTT_RESTART":
		ws_server.send(get_control_json("pTT_RESTART"))
		time.sleep(2)
		state = "pTT_PREMI"

	if state == "pTT_PREMI":
		ws_server.send(get_control_json("pTT_PREMI"))
		current_temp = 0
		temp_hold_attemps = MAX_TEMP_HOLD_ATTEMPS 
		state = "pTT_TEMP_CONTROL"
		return

	# if state == "pTT_PRE_TEMP_CONTROL":
	# 	state == "pTT_TEMP_CONTROL"		

	if state == "pTT_TEMP_CONTROL":
		current_temp += 1 + random.random()
		if current_temp >= MAX_TEMP:
			current_temp = MAX_TEMP
			state = "pTT_WAIT_FOR_HOLD"
			ws_server.send(get_control_json("pTT_TEMP_CONTROL", "{:.2f}".format(current_temp) ))
		else:
			ws_server.send(get_control_json("pTT_TEMP_CONTROL", "{:.2f}".format(current_temp) ))
			return

	if state == "pTT_WAIT_FOR_HOLD":
		ws_server.send(get_control_json("pTT_ASK_TO_HOLD"))
		time.sleep(3)
		send_to_leds(0, state)
		ws_server.send(get_control_json("pTT_ASK_TO_HOLD", "GO!"))
		time.sleep(0.2)
		send_to_leds(current_temp, state) 
		state = "pTT_ASK_TO_HOLD"
		return

	if state == "pTT_ASK_TO_HOLD":
		current_temp_holding_time = TEMP_HOLD_SECS
		ws_server.send(get_control_json("pTT_HOLDING", current_temp_holding_time))
		state = "pTT_HOLDING"
		t = math.pi/2
		return

	# if state == "pTT_HOLDING":
	# 	ws_server.send(get_control_json("pTT_HOLDING", current_temp_holding_time))

		# with cv:
		# 	cv.notify()




# ======  End of STATE MACHINE  =======


def button_handler(n):
	global cv, last_button_press
	global current_temp, state
	last_button_press = time.time()

	send_to_leds(current_temp, state)

	with cv:
		cv.notify()




def main():
	import argparse
	global sock

	parser = argparse.ArgumentParser(description='Explora\'s Il buono prima di tutto controller' )
	parser.add_argument('-t','--time',type=int, default=defaults.VIDEO_TIME,  help='Seconds on each round')
	parser.add_argument('--socket', '-s', nargs='?', default='/tmp/display-server-socket') 
	args = parser.parse_args()
	
	#START HTTP SERVER	
	http_server.start()
	dump_to_console("Explora\'s Il buono prima di tutto HTTP server started at port {}\non {}"\
		.format(http_server.PORT, datetime.datetime.now()))
	
	#START WS SERVER
	ws_server.start()
	dump_to_console("Explora\'s Il buono prima di tutto Websocket server started at port {}\non {}"\
		.format(ws_server.WS_PORT, datetime.datetime.now()))
	
	#Start ws2812 client
	sock = socket.socket(socket.AF_UNIX,
	                     socket.SOCK_DGRAM)

	sock.connect(args.socket)

	#button manager
	bman = btn_manager.Button_Manager()
	bman.set_handler(0, button_handler)

	#Automatic state modification
	t = threading.Thread(target=auto_mod_state)
	t.start()

	#HANDLE SIGTERM
	def close_sig_handler(signal, frame):
		dump_to_console("\nInterrupt signal received, cleaning up..")
		ws_server.close()
		http_server.close()
		global AUTO_MOD_DELAY
		AUTO_MOD_DELAY = 0
		t.join()
		exit()

	signal.signal(signal.SIGINT, close_sig_handler)

	
	ws_server.send(get_control_json("pTT_PREMI"))
	global state
	state = "pTT_TEMP_CONTROL"

	while 1:
		with cv:
			cv.wait()
		
		handle_state()

		
		

	# PLAY LOOP

	# while 1:
		
	# 	the_code = ""
	# 	while 1:
	# 		c = f.read(1)
	# 		if ord(c) == 40: break

	# 		if ord(c):
	# 			the_code += str(code_input_to_number(ord(c)))
	# 			stdout.flush()


	# 	db_error = code_to_db(the_code)
	# 	if db_error:
	# 		dump_to_console(db_error)
	# 		continue


	# 	dump_to_console("Scanned code", the_code)

	# 	ws_server.send(get_control_json("INSTRUCTIONS"))
	# 	# dump_to_console("INSTRUCTIONS screen, waiting ", delays["INSTRUCTIONS"], "seconds")
	# 	time.sleep(delays["INSTRUCTIONS"])

	# 	for i in range(delays["PRE_COUNT"]):
	# 		ws_server.send(get_control_json("PRE_COUNT", str(delays["PRE_COUNT"] - i)))
	# 		time.sleep(1)

	# 	ws_server.send(get_control_json("PLAY"))
	# 	# dump_to_console("PLAY screen, waiting ", delays["PRE_COUNT"], "seconds")
	# 	time.sleep(delays["PLAY"])

	# 	t = threading.Thread(target=countdown, args=(args.time,))
	# 	t.start()
	# 	# -----------  Start  -----------
		 
	# 	game = riflessi.Riflessi_Game()
	# 	for i,btn in enumerate(riflessi.buttons):
	# 		btn.when_pressed = game.press_button 
		
	# 	game.light_button()

	# 	# -----------  PLAY  -----------
		
	# 	t.join() #--- Block here ---

	# 	# -----------  END  -----------
	# 	ws_server.send(get_control_json("FINAL", str(game.score)))
	# 	del(game)
	# 	# dump_to_console("FINAL screen, waiting ", delays["FINAL"], "seconds")
		
	# 	time.sleep(delays["FINAL"])

	# 	for i,btn in enumerate(riflessi.buttons):
	# 		btn.when_pressed = riflessi.no_button 


	


if __name__ == '__main__':
	main()