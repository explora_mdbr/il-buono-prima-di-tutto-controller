import time
import socket
import argparse
from struct import pack
from sys import exit as die

# def auto_int(x):
#     return int(x, 0)

parser = argparse.ArgumentParser(description='Send an test message to an ws_2811 led strip display server.')

parser.add_argument('--socket', '-s', nargs='?', default='/tmp/display-server-socket') 
# t = time.time()
# parser.add_argument('message', nargs='*', default=[a*256/16 for a in range(24)], type=auto_int)
args = parser.parse_args()

# while len(args.message) != 24:
# 	args.message.append(0)


# MESSAGE = args.message

streamFormat = b"<" + b"I"*44

 
sock = socket.socket(socket.AF_UNIX,
                     socket.SOCK_DGRAM)

sock.connect(args.socket)

colors = [0xff0000, 0x00ff00, 0x0000ff]

MESSAGE = [colors[i%3] for i in range(22)] + ([0xffffff] * 22)


print "Sending to socket:", args.socket
print "Values: {}".format(MESSAGE)

import math

t = 0
flow_speed = 20

def get_control_led_milk_array(temp, flow_right = False, flow_left = False):
	global t
	temp_color = int(temp * 0xff)
	final_color = 0x000000 | (temp_color <<16) | (255-temp_color)
	final_color = min(final_color, 0xffffff)

	flow_right = [int(flow_right) * 0xffffff ] * 9
	flow_left = [int(flow_left) * 0xffffff ] * 9

	pos = int(t * flow_speed) % 9

	flow_left[pos] = 0
	flow_right[pos] = 0 

	return [final_color for i in range(22)] + flow_right + flow_left + [0]*4


import random
direction = True

while 1:
	if random.random() > 0.95:
		direction = not direction

	MESSAGE = get_control_led_milk_array( math.sin(t*2) * 0.5 + 0.5, direction, not direction )
	sock.send(pack(streamFormat, *MESSAGE))
	t += 0.1
	time.sleep(0.1)
	# if t >= 1 : break
	

